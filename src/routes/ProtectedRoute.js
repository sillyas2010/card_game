import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

export default
class ProtectedRoute extends Component {
    static propTypes = {
        component: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
    }

    render() {
        const {
          component: Component,
          condition,
          ...props
        } = this.props;

        return (
            <Route
              {...props}
              render={props => (
                condition ?
                  <Component {...props} {...this.props}/>
                  :
                  <Redirect to="/error"/>
              )}
            />
        )
    }
}
