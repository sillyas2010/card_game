import React, { PureComponent } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

export default
class ProtectedLink extends PureComponent {
    static propTypes = {
        to: PropTypes.oneOfType([PropTypes.string,PropTypes.object]).isRequired,
        condition: PropTypes.bool
    }

    state = {
        active: false
    }

    onClick = (e) => {
        e.preventDefault();

        if(this.props.onClick) {
            this.props.onClick(e);
        }

        this.setState({
            active: true
        });
    }

    render() {
        const {
            children,
            condition,
            ...props
        } = this.props;

        return (
            this.state.active && condition ?
                <Redirect {...props}/>
                :
                <a {...props} onClick={this.onClick}>
                    { children }
                </a>
        )
    }
}
