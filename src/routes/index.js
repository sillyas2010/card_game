import React, { Component } from 'react';
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';

import ProtectedRoute from './ProtectedRoute';
import PropsRoute from './PropsRoute';
import StartScreen from 'components/StartScreen';
import Error from 'components/Error';
import GameField from 'components/GameField';
import WinScreen from 'components/WinScreen';
import LossScreen from 'components/LossScreen';

export default
@inject('store')
class Routes extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                lost: PropTypes.bool.isRequired,
                won: PropTypes.bool.isRequired,
                inGame: PropTypes.bool.isRequired
            })
        })
    }

    render() {
        let {
          gameData
        } = this.props.store;

        return (
          <Switch>
            <ProtectedRoute
              exact
              path="/game"
              component={GameField}
              condition={gameData.inGame}
            />
            <ProtectedRoute
              exact
              path="/win"
              component={WinScreen}
              condition={gameData.won}
            />
            <ProtectedRoute
              exact
              path="/loss"
              component={LossScreen}
              condition={gameData.lost}
            />
            <Route
              exact
              path="/"
              component={StartScreen}
            />
            <PropsRoute
              component={Error}
              errorCode={404}
              errorText={'Oops! That page couldn\'t be found.'}
            />
          </Switch>
        )
    }
}
