import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import { AppContainer } from 'react-hot-loader';
import { configure } from 'mobx'
import { Provider } from 'mobx-react';

import Routes from 'routes';
import history from 'globalHistory';
import store from 'store';
import { STRICT_MODE } from 'common/const';
import 'common/scss/index.scss';


const root = document.getElementById('root');

const render = () => {
  ReactDOM.render(
    <AppContainer>
        <Provider store={store}>
            <Router history={history}>
                <Routes/>
            </Router>
        </Provider>
    </AppContainer>,
    root,
  );
};

configure({
    enforceActions: STRICT_MODE
});

render();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
