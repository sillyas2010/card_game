import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Notification.scss';

export default
class NotificationHeader extends Component {
    static propTypes = {
        isError: PropTypes.bool,
        errorCode: PropTypes.number
    }

    render() {
        return (
            <header className="Notification-header">
                { this.props.isError && 'Error' } { this.props.errorCode }
                { this.props.children }
            </header>
        );
    }
}
