import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Notification.scss';

export default
class Notification extends Component {
    static propTypes = {
        isError: PropTypes.bool
    }

    render() {

        let inheritClasses = typeof this.props.className === 'string' ? this.props.className.split(' ') : '',
            wrapperClass = classNames(
              'Notification-wrapper',
              { 'Notification-error': this.props.isError },
              ...inheritClasses
            );

        return (
            <section className={ wrapperClass }>
                { React.Children.map(this.props.children, child => React.cloneElement(child, { isError: this.props.isError, ...child.props }))}
            </section>
        );
    }
}

export { default as NotificationHeader } from './NotificationHeader';
export { default as NotificationContent } from './NotificationContent';
export { default as NotificationFooter } from './NotificationFooter';

