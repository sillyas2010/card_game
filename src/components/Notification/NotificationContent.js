import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Notification.scss';

export default
class NotificationContent extends Component {
    static propTypes = {
        className: PropTypes.string
    }

    render() {
        let inheritClasses = typeof this.props.className === 'string' ? this.props.className.split(' ') : '',
            wrapperClass = classNames(
                'Notification-content',
                ...inheritClasses
            );

        return (
            <main className={wrapperClass}>
                { this.props.children }
            </main>
        );
    }
}
