import React, { Component } from 'react';

import './Notification.scss';

export default
class NotificationFooter extends Component {
    render() {
        return (
            <footer className="Notification-footer">
                { this.props.children }
            </footer>
        );
    }
}
