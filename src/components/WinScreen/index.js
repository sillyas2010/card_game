import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import Notification, { NotificationHeader, NotificationContent, NotificationFooter } from 'components/Notification';
import LeaderBoard from 'components/LeaderBoard';

export default
@inject('store')
@observer
class WinScreen extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                stopGame: PropTypes.func.isRequired
            })
        })
    }

    render() {
        let {
            gameData
        } = this.props.store;

        return (
            <Notification>
                <NotificationHeader>
                    You won!
                </NotificationHeader>
                <NotificationContent>
                    Thank you for the game. Now you can restart.
                    But first here is the leaderboard, take a look.
                    <LeaderBoard/>
                </NotificationContent>
                <NotificationFooter>
                    <Link to="/" onClick={gameData.stopGame} className="btn animated on-click">Go to Homepage</Link>
                </NotificationFooter>
            </Notification>
        );
    }
}
