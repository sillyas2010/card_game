import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Notification, { NotificationContent, NotificationFooter } from 'components/Notification';
import ProtectedLink from 'routes/ProtectedLink';
import OptionsForm from 'components/OptionsForm';
import { makeResponsiveCol } from 'common/func';

export default
@inject('store')
@observer
class StartScreen extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                setOptions: PropTypes.func.isRequired,
                startGame: PropTypes.func.isRequired
            })
        })
    }

    state = {
        valid: false
    }

    optionsForm = React.createRef()

    startGame = () => {
      let form = this.optionsForm.current;

      if(form) {
          if(form.isValid) {
              let validated = form.isValid();

              if(validated.status) {
                  if(validated.name && validated.pairsNum) {
                      let nameVal = validated.name.value,
                          pairsVal = parseInt(validated.pairsNum.value);

                      this.props.store.gameData.setOptions(nameVal, pairsVal);

                      this.props.store.gameData.startGame();

                      this.setState({
                          valid: true
                      });
                  }
              }
          }
      }

    }

    render() {
        return (
            <Notification>
                <NotificationContent>
                    <div className="row">
                        <span className={classNames(
                            makeResponsiveCol({ 'all': '12' }),
                            'Notification-description'
                        )}>
                            Hello, here you can start the game.
                            Objective of the game is to match two cards
                            with the same shirt. Now please get started.
                            Type your nickname, number of card pairs and
                            hit the "Start" button.
                        </span>
                    </div>
                    <OptionsForm ref={this.optionsForm}/>
                </NotificationContent>
                <NotificationFooter>
                    <ProtectedLink
                        to="/game"
                        onClick={this.startGame}
                        condition={this.state.valid}
                        className="btn animated on-click">Start game</ProtectedLink>
                </NotificationFooter>
            </Notification>
        );
    }
}
