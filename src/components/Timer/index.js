import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { observer, inject } from 'mobx-react';

import { getTime } from 'common/func';
import './Timer.scss';

export default
@inject('store')
@observer
class Timer extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                timerSec: PropTypes.number.isRequired
            })
        })
    }

    getSnapshotBeforeUpdate() {
        let { timerSec, matchedAllCards, wonGame, lostGame } = this.props.store.gameData;

        // set win if all cards matched, to prevent loss
        if(timerSec === 0 && matchedAllCards) {
            wonGame();
        }

        if(timerSec === -1 && !matchedAllCards) {
            lostGame();
        }

        return null;
    }

    render() {
        let {
            gameData
        } = this.props.store,
        time = getTime(gameData.timerSec),
        almostOutOfTime = gameData.timerSec <= 10;

        return (
            <article className={classNames('timer-wrapper', { 'almost-out-of-time': almostOutOfTime })}>
                <span className={'timer-hours'}>{ time.hours }</span>
                <span className={'timer-minutes'}>{ time.minutes }</span>
                <span className={'timer-seconds'}>{ time.seconds }</span>
            </article>
        );
    }
}
