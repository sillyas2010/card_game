import React, { Component } from 'react';

import Notification, { NotificationContent } from 'components/Notification';
import Cards from 'components/Cards';
import Timer from 'components/Timer';
import './GameField.scss';

export default
class GameField extends Component {
    render() {
        return ([
            <Notification className={'GameField-wrapper'} key={'GameField'}>
                <NotificationContent className={'GameField-content container row'}>
                    <Cards/>
                </NotificationContent>
            </Notification>,
            <Timer key={'Timer'}/>
        ]);
    }
}
