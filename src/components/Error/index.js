import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Notification, { NotificationHeader, NotificationContent, NotificationFooter } from 'components/Notification';

export default
@inject('store')
@observer
class Error extends Component {
    static propTypes = {
        errorCode: PropTypes.number.isRequired,
        errorText: PropTypes.string.isRequired,
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                stopGame: PropTypes.func.isRequired
            })
        })
    }

    render() {
    let {
        gameData
    } = this.props.store;

    return (
      <Notification isError={true}>
          <NotificationHeader errorCode={this.props.errorCode}/>
          <NotificationContent>
              { this.props.errorText }
          </NotificationContent>
          <NotificationFooter>
              <Link to="/" onClick={gameData.stopGame} className="btn animated on-click">Go to Homepage</Link>
          </NotificationFooter>
      </Notification>
    );
    }
}
