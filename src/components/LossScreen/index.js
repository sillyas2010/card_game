import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import Notification, { NotificationHeader, NotificationContent, NotificationFooter } from 'components/Notification';

export default
@inject('store')
@observer
class LossScreen extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                stopGame: PropTypes.func.isRequired
            })
        })
    }

    render() {
        let {
            gameData
        } = this.props.store;

        return (
            <Notification>
                <NotificationHeader>
                    You lost.
                </NotificationHeader>
                <NotificationContent>
                    Anyway thank you for the game.
                    Try again, we wish you a better luck.
                </NotificationContent>
                <NotificationFooter>
                    <Link to="/" onClick={gameData.stopGame} className="btn animated on-click">Go to Homepage</Link>
                </NotificationFooter>
            </Notification>
        );
    }
}
