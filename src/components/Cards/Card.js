import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { observer, inject } from 'mobx-react';

import { CARD_FRONT_IMG } from 'common/const';
import { makeResponsiveCol } from 'common/func';

export default
@inject('store')
@observer
class Card extends Component {
    static propTypes = {
        ind: PropTypes.number.isRequired,
        src: PropTypes.string.isRequired,
        flipped: PropTypes.bool.isRequired,
        cards: PropTypes.array,
        prevFlippedInd: PropTypes.number,
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                flipCard: PropTypes.func.isRequired
            })
        })
    }

    onCardFlip = () => {
      this.props.store.gameData.flipCard(this.props.ind,this.props.flipped);
    }

    render() {
        let { ind, src, flipped, disabled } = this.props;

        return (
            <article
              data-ind={ind}
              className={classNames(
                  'Card-wrapper animated on-click',
                  { 'disabled': disabled },
                  { 'flipped': flipped },
                  makeResponsiveCol({ 'all': '12', 'sm': '6', 'lg': '4', 'xl': 3 })
              )}
              { ...!disabled && ({onClick: this.onCardFlip}) }
            >
                <div className="Card-front">
                    <div
                      className="img-wrapper"
                      style={{ backgroundImage: `url(/assets/img/${CARD_FRONT_IMG})` }}
                    />
                </div>
                <div className="Card-back">
                    <div
                      className="img-wrapper"
                      style={{ backgroundImage: flipped && `url(/assets/img/${src})` }}
                    />
                </div>
            </article>
        );
    }
}
