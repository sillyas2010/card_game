import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';

import Card from './Card';
import './Cards.scss';
import { CARDS_IMG_ARR } from 'common/const';
import { arrayShuffle } from 'common/func';

export default
@inject('store')
@observer
class Cards extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                cardsMapped: PropTypes.func.isRequired,
                pairsNum: PropTypes.number.isRequired,
                cards: PropTypes.array.isRequired
            })
        })
    }

    componentDidMount() {
        this.props.store.gameData.cardsMapped(this.mapCardsArray());
    }

    mapCardsArray() {
          // We shuffling cards array and leaving amount which we need (based on user option)
        let cardsArray = arrayShuffle(CARDS_IMG_ARR).splice(0, this.props.store.gameData.pairsNum),
          // We duplicating images from images array (for making pairs)
          srcArray = [ ...cardsArray, ...cardsArray ].map( src => ({src}) );

        return srcArray.map( (card, ind, arr) => ({
            src: card.src,
            flipped: false
        }));
    }

    render() {
        return (
            this.props.store.gameData.cards.map( (card, cardInd) => (
                <Card
                  key={cardInd}
                  ind={cardInd}
                  src={card.src}
                  flipped={card.flipped}
                  disabled={card.disabled}
                />
            )) || ''
        );
    }
}
