import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';

import { LEADERBOARD_LIMIT } from 'common/const';
import { getTime, makeImmutable } from 'common/func';
import './LeaderBoard.scss';

export default
@inject('store')
@observer
class LeaderBoard extends Component {
    static propTypes = {
        store: PropTypes.shape({
            gameData: PropTypes.shape({
                leaderBoard: PropTypes.array.isRequired,
                pairsNum: PropTypes.number.isRequired
            })
        })
    }

    render() {
        let {
            gameData
        } = this.props.store,
        // get leaderBoard, filter only current user's number of pairs, sort it, and limit displaying
        sortedBoard = makeImmutable(gameData.leaderBoard)
            .filter(el => el.pairsNum === gameData.pairsNum)
            .sort((a, b) => b.timeLeft - a.timeLeft)
            .splice(0, LEADERBOARD_LIMIT);

        return (
            <table className={'leaderBoard-wrapper'}>
                <thead>
                    <tr>
                        <th>Ind</th>
                        <th>Nickname</th>
                        <th>Pairs Num</th>
                        <th>Time Left</th>
                    </tr>
                </thead>
                <tbody>
                { sortedBoard.map( (el,ind) =>  {
                    let leaderTime = getTime(el.timeLeft);

                    return (
                    <tr className={'leaderBoard-item'} key={ind}>
                        <td>{ ind + 1 }</td>
                        <td>{ el.name }</td>
                        <td>{ el.pairsNum }</td>
                        <td>{ leaderTime.hours }:{ leaderTime.minutes }:{ leaderTime.seconds }</td>
                    </tr>
                    )
                }) }
                </tbody>
            </table>
        );
    }
}
