import React, { Component } from 'react';
import classNames from 'classnames';

import { makeResponsiveCol } from 'common/func';
import { CARDS_IMG_ARR } from 'common/const';

import Input from './Input';

export default
class OptionsForm extends Component {
    constructor() {
        super();

        let hasName = 'name' in localStorage,
            name = hasName ? localStorage.getItem('name') : '',
            hasPairsNum = 'pairsNum' in localStorage,
            pairsNum = hasPairsNum ? localStorage.getItem('pairsNum') : '';

        this.state = {
            name: {
                value: name,
                validity: { valid: true },
                touched: hasName
            },
            pairsNum: {
                value: pairsNum,
                validity: { valid: true },
                touched: hasPairsNum
            }
        };
    }

    isValid = (e) => {
        let allValid = true,
            newState = { ...this.state };

        if(e) {
            let el = e.target;

            newState[el.name].validity = { valid: newState[el.name].touched && el.validity.valid };
        } else {
            Object.keys(this.state).forEach(key => {
                let el = this.state[key];

                newState[key].validity.valid = el.touched && el.validity.valid;

                if(allValid) {
                    allValid = el.touched && el.validity.valid
                }
            });
        }

        this.setState(newState);

        return { status: allValid, ...newState };
    }

    onInputChange = (e) => {
        let el = e.target,
            newState = {};

        newState[el.name] = {
            value: el.value,
            validity: el.validity,
            touched: true
        };

        this.setState(newState);
    }

    render() {
        let { name, pairsNum } = this.state,
            maxCardsLength = CARDS_IMG_ARR.length,
            labelClass = classNames(
                makeResponsiveCol({ 'all': '12', 'md': '6' })
            );

        return (
            <form action="#" className="form row">
                <Input
                    placeholder={'4 < length < 16'}
                    onChange={this.onInputChange}
                    onBlur={this.isValid}
                    labelClass={labelClass}
                    className={'input'}
                    value={name.value}
                    isValid={name.validity.valid}
                    required={true}
                    minLength={3}
                    maxLength={15}
                    type={'text'}
                    name={'name'}
                >Name</Input>
                <Input
                    placeholder={'0 < x < 7'}
                    onChange={this.onInputChange}
                    onBlur={this.isValid}
                    labelClass={labelClass}
                    className={'input'}
                    value={pairsNum.value}
                    isValid={pairsNum.validity.valid}
                    required={true}
                    type={'number'}
                    name={'pairsNum'}
                    min={1}
                    max={maxCardsLength}
                >Pairs num</Input>
            </form>
        );
    }
}
