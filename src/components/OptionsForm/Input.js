import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default
class Input extends PureComponent {
    static propTypes = {
        type: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        labelClass: PropTypes.string.isRequired,
        isValid: PropTypes.bool
    }

    render() {
        let {
            children,
            labelClass,
            className,
            isValid,
            ...props
        } = this.props,
        invalid = isValid !== undefined ? !isValid : false,
        invalidClass = invalid ? 'invalid ' : '';

        return (
            <label className={ `input-label ${invalidClass}${labelClass}` }>
                { children }
                <input { ...props } className={ `input ${className}` }/>
            </label>
        )
    }
}
