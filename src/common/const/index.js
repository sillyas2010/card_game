export const STRICT_MODE = 'observed';
export const ACTION_DELAY = 1500;
export const TIME_PER_PAIR = 10;
export const DIFFICULTY_MULTIPLIER = 0.7;
export const LEADERBOARD_LIMIT = 5;
export const CARD_FRONT_IMG = 'chrome.svg';
export const CARDS_IMG_ARR = [
  'angular.svg','aurelia.svg','backbone.svg','ember.svg','react.svg','vue.svg'
];