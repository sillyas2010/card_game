export const makeResponsiveCol = breakpointsObj => {
    let classString = 'col';

    Object.keys(breakpointsObj).forEach(key => {
        let breakpoint = `-${key}`;
        classString += ` col${breakpoint === '-all' ? '' : breakpoint}-${breakpointsObj[key]}`;
    });

    return classString;
};

export const getTime = secs => {
    let sec_num = parseInt(secs >= 0 ? secs : 0),
        timeObj = {};

    [
        { key: 'hours',   value: Math.floor(sec_num / 3600) % 24 },
        { key: 'minutes', value: Math.floor(sec_num / 60) % 60 },
        { key: 'seconds', value: sec_num % 60 }
    ].forEach( ({ key, value }) => {
        timeObj[key] = value < 10 ? `0${value}` : value;
    });

    return timeObj;
};

export const makeImmutable = arrOrObj => JSON.parse(JSON.stringify(arrOrObj))

export const arrayShuffle = array => {
    let resArr = makeImmutable(array), currentIndex = resArr.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = resArr[currentIndex];
        resArr[currentIndex] = resArr[randomIndex];
        resArr[randomIndex] = temporaryValue;
    }

    return resArr;
};