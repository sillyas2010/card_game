import history from 'globalHistory';
import { TIME_PER_PAIR, DIFFICULTY_MULTIPLIER, ACTION_DELAY } from 'common/const';

export const startGameAction = function() {
  this.cards.clear();
  this.inGame = true;
  this.matchedAllCards = false;
  this.won = false;
  this.lost = false;
  this.startTimer();

  if('leaderBoard' in localStorage) {
      this.leaderBoard = JSON.parse(
          localStorage.getItem('leaderBoard')
      );
  }
};

export const stopGameAction = function() {
  this.cards.clear();
  this.inGame = false;
  this.won = false;
  this.lost = false;
};

export const setOptionsAction = function (name, pairsNum) {
  this.name = name;
  this.pairsNum = pairsNum;

  localStorage.setItem('name', name);
  localStorage.setItem('pairsNum', pairsNum);
};

export const startTimerAction = function () {
  this.timerSec = TIME_PER_PAIR * this.pairsNum * DIFFICULTY_MULTIPLIER;

  this.timerInd = setInterval(() => {
     this.decrementTimer();
  },1000);
};

export const decrementTimerAction = function () {
  // -1 value of timer would show the loss status
  if(this.timerSec >= 0) {
      this.timerSec--;
  }
};

export const stopTimerAction = function () {
  clearInterval(this.timerInd);
};

export const cardsMappedAction = function(cards) {
  this.cards = this.cards.concat(cards);
};

export const flipCardAction = function(ind, flipped) {
    let flipIndex = ind,
      currentFlipped = !flipped,
      firstFlipInPair = typeof this.prevFlippedInd === 'undefined',
      pairMatched = !firstFlipInPair &&
          flipIndex !== this.prevFlippedInd &&
          this.cards[flipIndex].src === this.cards[this.prevFlippedInd].src;

    this.cards[flipIndex].flipped = currentFlipped;

    if(!firstFlipInPair) {
        let prevFlippedInd = this.prevFlippedInd;

        if(pairMatched) {
            this.cards[prevFlippedInd].disabled = true;
            this.cards[flipIndex].disabled = true;
        }

        this.twoFlipped(prevFlippedInd,flipIndex, pairMatched);
    }

    if(firstFlipInPair && currentFlipped) {
        this.prevFlippedInd = ind;
    } else {
        this.prevFlippedInd = undefined;
    }
};

export const twoFlippedAction = function (ind1, ind2, pairMatched) {
    if(pairMatched) {
      let gameWon = this.cards.every(card => card.flipped);

      this.pairMatched(ind1, ind2, pairMatched);

      gameWon && this.delayedWin();
    } else {
      setTimeout(() => {
        // prevent undefined cards match after timer end
        if(!this.lost) {
            this.pairMatched(ind1, ind2, pairMatched);
        }
      }, ACTION_DELAY );
    }
};

export const pairMatchedAction = function (ind1, ind2, isMatched) {
    this.cards[ind1].flipped = isMatched;
    this.cards[ind2].flipped = isMatched;
};

export const delayedWinAction = function () {
    this.matchedAllCards = true;

    setTimeout(() => {
        // prevent setting win action after timer end
        if(!this.won) {
            this.wonGame();
        }
    }, ACTION_DELAY);

};

export const wonGameAction = function() {
      this.cards.clear();
      this.won = true;
      this.inGame = false;
      this.stopTimer();

      this.leaderBoard.push({
          name: this.name,
          pairsNum: this.pairsNum,
          timeLeft: this.timerSec
      });

      localStorage.setItem(
          'leaderBoard',
          JSON.stringify(this.leaderBoard)
      );

      history.push('/win');
};

export const lostGameAction = function() {
  this.cards.clear();
  this.lost = true;
  this.inGame = false;
  this.stopTimer();

  history.push('/loss');
};