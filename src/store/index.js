import { observable, action } from 'mobx';

import {
    startGameAction,
    stopGameAction,
    setOptionsAction,
    startTimerAction,
    decrementTimerAction,
    stopTimerAction,
    cardsMappedAction,
    flipCardAction,
    twoFlippedAction,
    pairMatchedAction,
    delayedWinAction,
    wonGameAction,
    lostGameAction
} from 'actions';

class GameData {
    @observable inGame = false;
    @action.bound startGame = startGameAction;
    @action.bound stopGame = stopGameAction;

    @observable name = '';
    @observable pairsNum = undefined;
    @observable leaderBoard = [];
    @action.bound setOptions = setOptionsAction;

    @observable timerSec = undefined;
    @observable timerInd = undefined;
    @action.bound startTimer = startTimerAction;
    @action.bound decrementTimer = decrementTimerAction;
    @action.bound stopTimer = stopTimerAction;

    @observable cards = [];
    @observable prevFlippedInd = undefined;
    @action.bound cardsMapped = cardsMappedAction;
    @action.bound flipCard = flipCardAction;
    @action.bound twoFlipped = twoFlippedAction;
    @action.bound pairMatched = pairMatchedAction;

    @observable won = false;
    @observable matchedAllCards = false;
    @observable lost = false;
    @action.bound delayedWin = delayedWinAction;
    @action.bound wonGame = wonGameAction;
    @action.bound lostGame = lostGameAction;
}

class RootStore {
    constructor() {
        this.gameData = new GameData();
    }
}

export default new RootStore();