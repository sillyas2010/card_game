const { override, addDecoratorsLegacy, disableEsLint } = require("customize-cra");
const { compose } = require('react-app-rewired');

module.exports = override(
    addDecoratorsLegacy(),
);